package com.serebit.strife.buildsrc

object ProjectInfo {
    const val name: String = "strife"
    const val group: String = "com.serebit.$name"
    const val version: String = "0.3.0"
    const val description: String = "An idiomatic Kotlin implementation of the Discord API"
}
