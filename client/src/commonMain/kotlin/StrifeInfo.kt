package com.serebit.strife

/**
 * An object containing information about the Strife library.
 */
object StrifeInfo {
    /** The link to the source repository on GitLab. */
    const val sourceUri: String = "https://gitlab.com/serebit/strife"
    /** The link to the project logo, which is hosted on GitLab. */
    const val logoUri: String =
        "https://assets.gitlab-static.net/uploads/-/system/project/avatar/6502506/strife-logo.png"
    /** The current version of Strife. */
    const val version: String = "0.3.0"
}
